# Mutatron

Mutatron is a research project using both Bayesian statistics and Mutation Testing. Its aim is to prioritize a test sequence in order to execute first tests
which are more prone to fail. To do that, Mutatron first go through a learning phase during which links between source code and tests are learnt.

## Test Mutatron behavior on a fake project

In order to test Mutatron core algorithm efficiency, it is possible to generate a "link table" containing hypothetical links between an imaginary source
code and an imaginary test base. An element of this table is a boolean specifying if the row test would fail if the column code element is modified.
Also, a fake duration list is generated, the number at i-th position being the hypothetical time in seconds taken by the i-th test when it is executed.

This setup makes it possible to test the theoretical foundations of Mutatron without having to spend hours running tests for real projects.

## Installing requirements

To install everything required by the project, it is highly advised to use a Python virtual environment manager. When in you virtual environment,
you can run the following command :

```shell
pip install -r requirements.txt
```

## Generating fake data

To generate a pool of fake data, go to this repository root and run the following command :

```shell
python scripts/fake_project/extract_learning_data.py
```

This will execute the two main steps of Mutatron on a fake project:
1. Learn the links between source code and tests by randomly selecting code elements to "modify", then checking which test "fail". The check basically consists in getting the corresponding boolean value in the fake project data table.
2. Get execution data by generating 1000 false commits, optimizing test orders, then computing time costs considering that the execution was to be stopped after the first test fail.

The results of this scripts are stored in the `experiment_data/fake_project` folder, along with the data got on 5 real Java project extracted from the Java Commons project.

## Analyzing generated data

To analyze both generated fake data and real data obtained on the 5 Java projects, first you have to run the Jupyter Lab server by executing this command in a terminal :

```shell
jupyter-lab
```

Then, on your Jupyter Lab interface, you have to open the "Analyze results.ipynb" notebook located in the `notebooks` folder.
Running the entire notebook will display all the relevant statistics that you might need in order to reproduce paper results.
