import os
import sys

try:
    # Project and files
    PROJECT_PATH = os.environ['PROJECT_PATH']
    COPIED_PROJECT_PATH = './target_source'

    # PostgreSQL
    POSTGRE_HOST = os.environ['POSTGRE_HOST']
    POSTGRE_MASTER_USERNAME = os.environ['POSTGRE_MASTER_USERNAME']
    POSTGRE_MASTER_PASSWORD = os.environ['POSTGRE_MASTER_PASSWORD']

    # Run shell command output
    MUTE_SHELL = os.environ['MUTE_SHELL'] == 'true'
    MUTE_PRINT = os.environ['MUTE_PRINT'] == 'true'

    # Learning hyperparameters
    RATIO_MODIFIED_ELEMENTS = float(os.environ['RATIO_MODIFIED_ELEMENTS'])
    N_MUTANTS_PER_FILE = int(os.environ['N_MUTANTS_PER_FILE'])
    N_ELEMENTS_MODIFIED_PER_MUTANTS = int(os.environ['N_ELEMENTS_MODIFIED_PER_MUTANTS'])

    # Number of tests to run
    N_EXECUTED_TESTS = int(os.environ['N_EXECUTED_TESTS'])

    # Constants
    TEST_TIMEOUT_FACTOR = float(os.environ['TEST_TIMEOUT_FACTOR'])
    P_DEV_ERROR = float(os.environ['P_DEV_ERROR'])
    EPSILON = 0.00000001

except KeyError as excp:
    print(f'Environment variable \'{excp.args[0]}\' is missing. Have you sourced the mutatron.env file ?')
    sys.exit(0)
