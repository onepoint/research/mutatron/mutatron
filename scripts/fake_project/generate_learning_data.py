import datetime
import json
import os
import pathlib
import random
import sys

from dotenv import load_dotenv

sys.path.insert(0, str(pathlib.Path('.').absolute()))
load_dotenv("mutatron.env")

from scripts.fake_project.config_fake_project import N_ELEMENTS, N_MODIFIED_ELEMENTS_COMMIT
from scripts.fake_project.config_fake_project import N_ITERATIONS as n_iter
from scripts.fake_project.learn import generate_learning_data, extract_test_results, name_to_id
from src.mutatron import Mutatron

random.seed(42)

data_folder: str = "experiment_data/fake_project"
data_folder = str(pathlib.Path(os.getcwd()) / data_folder)
if os.path.isdir(data_folder):
    os.system(f'rm -rf {data_folder}')
os.system(f'mkdir {data_folder}')

data_connector = generate_learning_data()

print('Initializing Mutatron')
mutatron = Mutatron(data_connector)
n_elements = mutatron.n_elements

print('Launching experiments')
for iteration in range(n_iter):
    if (iteration+1) % 100 == 0:
        print(f'Iteration {iteration+1}')
    modified_element_ids = random.sample(range(1, N_ELEMENTS+1), N_MODIFIED_ELEMENTS_COMMIT)
    test_results = extract_test_results(modified_element_ids)

    failing_tests = [test['name'] for test in test_results if test['result'] == 'fail']
    failing_tests = [name_to_id(test_name) for test_name in failing_tests]

    ordering = mutatron.optimize(index_modified_elements=modified_element_ids)

    real_cost = mutatron.compute_real_cost(
        test_ordering=list(range(1, mutatron.n_tests + 1)),
        failing_tests=failing_tests
    )

    optimized_cost = mutatron.compute_real_cost(
        test_ordering=ordering,
        failing_tests=failing_tests
    )

    result = {
        'real_cost': int(real_cost),
        'optimized_cost': int(optimized_cost),
        'modified_element_ids': modified_element_ids,
        'ordering': ordering,
        'failing_tests': failing_tests
    }

    timestamp: int = int(datetime.datetime.now().timestamp())
    filename = str(pathlib.Path(data_folder) / f'data_{timestamp}{random.randint(0, 100000)}.json')
    with open(filename, 'w') as data_file:
        json.dump(result, data_file)
