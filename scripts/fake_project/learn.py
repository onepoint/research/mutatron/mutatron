import pathlib
import random
import sys
from typing import List

import numpy as np

from scripts.fake_project.config_fake_project import N_SAMPLE, N_ELEMENTS, N_MODIFIED_ELEMENTS_LEARN, N_TESTS, DURATIONS, \
    N_ELEMENTS_PER_TEST
from src.repository.connectors.DataConnector import DataConnector
from src.repository.connectors.FakeProjectDataConnector import FakeProjectDataConnector

sys.path.insert(0, str(pathlib.Path('..').absolute()))

data = np.zeros((N_TESTS, N_ELEMENTS))
for i in range(N_TESTS):
    data[i][np.random.randint(0, N_ELEMENTS, N_ELEMENTS_PER_TEST)] = True


def check_success(test_id, mutant_element_ids):
    for element_id in mutant_element_ids:
        if data[test_id-1, element_id-1]:
            return 'fail'
    return 'success'


def extract_test_results(mutant_element_ids):
    results = [
        {
            'name': id_to_name(test_id, 'test'),
            'result': check_success(test_id, mutant_element_ids),
            'duration': DURATIONS[test_id-1]
        }
        for test_id in range(1, N_TESTS+1)
    ]
    return results


def ids_to_names(indices, prefix):
    return [id_to_name(indice, prefix) for indice in indices]


def id_to_name(indice, prefix):
    return f'{prefix}_{indice}'


def name_to_id(name):
    return int(name.split('_')[1])


def generate_learning_data() -> DataConnector:
    data_connector: FakeProjectDataConnector = FakeProjectDataConnector(N_TESTS, N_ELEMENTS, DURATIONS)

    print("Generating learning data")
    for i in range(N_SAMPLE):
        if (i + 1) % 100 == 0:
            print(f'Sample {i + 1}/{N_SAMPLE}')

        modified_elements_ids = random.sample(range(1, N_ELEMENTS+1), N_MODIFIED_ELEMENTS_LEARN)
        test_results = extract_test_results(modified_elements_ids)

        # Update database
        test_names: List[str] = [test['name'] for test in test_results]
        failed_test_names: List[str] = [test['name'] for test in test_results if test['result'] != 'success']

        # Retrieve item IDs
        failed_test_ids_offseted: List[int] = [name_to_id(name)-1 for name in failed_test_names]
        test_executed_ids_offseted: List[int] = [name_to_id(name)-1 for name in test_names]
        modified_elements_ids_offseted: List[int] = [x-1 for x in modified_elements_ids]

        # Update data
        data_connector.test_fails[failed_test_ids_offseted] += 1
        for x in failed_test_ids_offseted:
            for y in modified_elements_ids_offseted:
                data_connector.test_element_fails[(x, y)] += 1
        data_connector.element_counters[modified_elements_ids_offseted] += 1
        data_connector.test_counters[test_executed_ids_offseted] += 1

    return data_connector


if __name__ == '__main__':
    data_connector = generate_learning_data()
    print("Done")
