from typing import List


class Element:
    def __init__(self, funcname: str, parent_name: str, params: List[str], filename: str):
        self.funcname: str = funcname
        self.parent_name: str = parent_name
        self.params: List[str] = params
        self.filename: str = filename

    def __str__(self):
        signature: str = f'{self.funcname}({", ".join(self.params)})'
        element_name: str = f'{self.filename}::{self.parent_name}::{signature}'
        return element_name


#==================== LEGACY ====================#
    # def get_regex(self) -> str:
    #     """
    #     This is the core function which enable the use of the `git log -L <start>,<end>:<filename>` command.
    #     It describes the regex required in the <start>,<end> part of the command.
    #     Many things has been tested. So far, the only thing working properly is to concat
    #     the function name along with its parameters, separated by `.*`.
    #     Many variations have been tested, the most important being:
    #     - Positive lookahead [(?=.*parameter)] to consider parameters in any order
    #     - Alternation [(option1|option2)] also to remove parameter order
    #     However, neither of these solutions are included in the POSIX BRE engine
    #     (see https://gist.github.com/CMCDragonkai/6c933f4a7d713ef712145c5eb94a1816) for further details).
    #     The limitation of the solution currently being used is that parameters have to be provided the exact same
    #     order as they are in the method definition. Another limitation, this time linked to the javaland python
    #     library, is that we can't retrieve array specifier (String[] args for example). To circumvent that
    #     parameters are joined using `.*`.
    #
    #     :return: The Element related regex to work with git log -L
    #     :rtype: str
    #     """
    #     updated_params: List[str] = ['.*'.join(param.split(' ')) for param in self.params]
    #     param_regex: str = '.*'.join(updated_params)
    #     regex_start: str = f'/^.*{self.funcname}.*{param_regex}.*$/'
    #     regex_end: str = '/\s*}/'
    #     return f"'{regex_start}','{regex_end}'"
#================================================#
