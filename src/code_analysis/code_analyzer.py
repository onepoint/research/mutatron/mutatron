import io
import os
import pathlib
from typing import List, Union

import javalang
from javalang.ast import Node
from javalang.tree import MethodDeclaration, ClassDeclaration, InterfaceDeclaration

from src.code_analysis.Element import Element
from src.types import ClassNode


def get_list_java_files_in_directory(path='.') -> List[str]:
    """
    Return a list of all java files that can be found in a directory.
    Default behavior is to search the current working directory.

    :param path: Directory path
    :type path: str
    :return: List of all java files paths
    :rtype: List[str]
    """
    elements: List[str] = []
    root: str
    files: List[str]
    for root, _, files in os.walk(path):
        if 'test' not in root:
            file: str
            for file in files:
                if pathlib.Path(file).suffix == '.java':
                    file_path = str(pathlib.Path(os.path.join(root, file)))
                    elements.append(file_path)
    return elements


def _extract_element_nodes_from_class_node(class_node: ClassNode) -> List[MethodDeclaration]:
    """
    Extract all element nodes from a single class node.
    Rather than using javalang.tree.Node.filter, we use a simple loop in node.body in order to avoid retrieving
    methods defined in subclasses of the given class node. It also fasten the search.

    :param class_node: Class node to investigate
    :type class_node: ClassNode
    :return: All element nodes found in input class node.
    :rtype: List[MethodDeclaration]
    """
    element_nodes: List[MethodDeclaration] = [
        node for node in class_node.body
        if isinstance(node, MethodDeclaration)
    ]
    for node in element_nodes:
        node.parent_class_name = class_node.name
    return element_nodes


def get_list_element_nodes(filename: Union[pathlib.Path, str]) -> List[MethodDeclaration]:
    """
    Return a list of javalang.tree.MethodDeclaration nodes, those nodes being the basic element required by Mutatron.

    :param filename: Filename to extract elements from.
    :type filename: Union[pathlib.Path, str]
    :return: A list of MethodDeclaration nodes.
    :rtype: List[javalang.tree.MethodDeclaration]
    """
    file: io.StringIO
    with io.open(filename, 'r') as file:
        tree: javalang.tree.CompilationUnit = javalang.parse.parse(file.read())
        _update_tree_with_positions(tree)
        class_nodes: List[ClassNode] = _extract_class_nodes_from_tree(tree)
        element_nodes: List[MethodDeclaration] = _extract_element_nodes_from_class_nodes(class_nodes)
        return element_nodes


def _update_tree_with_positions(node: Union[Node, list, tuple], position=None):
    children = None

    if isinstance(node, Node):
        children = node.children
        if node.position:
            position = node.position
        else:
            node._position = position
    else:
        children = node

    for child in children:
        if isinstance(child, (Node, list, tuple)):
            if isinstance(child, Node):
                if not child.position:
                    child._position = position
            _update_tree_with_positions(child, position)


def _extract_element_nodes_from_class_nodes(class_nodes: List[ClassNode]) -> List[MethodDeclaration]:
    """
    Extract all element nodes from a list of class nodes

    :param class_nodes: List of target class nodes
    :type class_nodes: List[ClassNode]
    :return: List of all element nodes found in input class nodes list
    :rtype: List[MethodDeclaration]
    """
    element_nodes: List[MethodDeclaration] = []
    class_node: ClassNode
    for class_node in class_nodes:
        element_nodes += _extract_element_nodes_from_class_node(class_node)
    return element_nodes


def _extract_class_nodes_from_tree(tree: javalang.tree.CompilationUnit) -> List[ClassNode]:
    """
    Extract all class nodes found in the input tree.

    :param tree: Original program tree.
    :type tree: javalang.tree.CompilationUnit
    :return: List of all class nodes found in the input tree.
    :rtype: List[ClassNode]
    """
    class_nodes: List[ClassNode] = []
    node_class: ClassNode
    for node_class in [ClassDeclaration, InterfaceDeclaration]:
        node: ClassNode
        class_nodes += [node for _, node in tree.filter(node_class)]
    return class_nodes


def get_list_elements(filename) -> List[Element]:
    """
    Return a list of Element, on for each item of the javalang.tree.MethodDeclarations nodes list.

    :param filename: File to extract elements from.
    :type filename: str
    :return: List of code elements
    :rtype: List[Element]
    """
    list_definitions: List[Element] = []
    list_elements_nodes: List[MethodDeclaration] = get_list_element_nodes(filename)
    # list_elements_nodes = _filter_override_nodes(list_elements_nodes)
    for node in list_elements_nodes:
        element: Element = extract_element_from_node(node, filename)
        list_definitions.append(element)
    return list_definitions


def extract_element_from_node(node: MethodDeclaration, filename: str) -> Element:
    """
    Extract an Element object based on the javalang.tree.MethodDeclaration node.

    :param node: The element node to convert into Element.
    :type node: javalang.tree.MethodDeclaration
    :param filename: Element origin filename
    :type filename: str
    :return: The corresponding Element object.
    :rtype: Element
    """
    funcname: str = node.name
    params: List[str] = [f'{param.type.name} {param.name}' for param in node.parameters]
    return Element(
        funcname=funcname,
        parent_name=node.parent_class_name,
        params=params,
        filename=filename
    )
