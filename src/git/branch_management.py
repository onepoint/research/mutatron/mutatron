import random

import pygit2 as git

from src.os_commands import run


def switch_to_mutant_branch() -> None:
    """
    Switch to a mutant branch on which files can be safely modified.
    """
    branch_name: str = f'mutant_{random.randint(100000, 1000000)}'
    run(f'git checkout -b {branch_name}')


def get_current_branch_name() -> str:
    """
    Get current git branch name.

    :return: Current git branch name.
    :rtype:  str
    """
    repo: git.Repository = git.Repository('.')
    name: str = repo.head.name
    name: str = name.split('/')[-1]
    return name


def switch_to_branch(branch: str = 'master') -> None:
    """
    Switch back to master and clean everything mutant-related.
    """
    mutant_branch_name: str = get_current_branch_name()
    if mutant_branch_name != branch:
        run(f'git checkout {branch}')
        run(f'git reset --hard {branch}')
        run('git clean -d --force')
        run(f'git branch -D {mutant_branch_name}')
