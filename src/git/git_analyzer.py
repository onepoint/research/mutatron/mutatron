"""
Module git qui permet de récupérer les tags des éléments de code modifiés.
Pour l'instant, on récupère l'ensemble des noms des fonctions avec leurs paramètres et leurs fichiers de définition
pour être sûr que chaque élément est identifié de manière unique.
"""
from __future__ import annotations

import io
import os
from typing import List, TYPE_CHECKING

import pygit2 as git

from src.code_analysis import code_analyzer
from src.os_commands import commands, run

if TYPE_CHECKING:
    from src.code_analysis.Element import Element
    from pygit2._pygit2 import Commit


def _get_list_modified_files() -> List[str]:
    """
    Return a list of all the files modified during the last commit.

    :return: List of modified files.
    :rtype: List[str]
    """
    hidden_filename: str = '.list_modified_files'

    run(f'git diff --name-only HEAD^ >> {hidden_filename}')
    with io.open(hidden_filename) as file:
        filename_list: List[str] = [filename[:-1] for filename in file.readlines()]

    run(f'{commands["remove"]} {hidden_filename}')

    return filename_list


def get_list_modified_elements_last_commit() -> List[str]:
    elements: List[str] = []

    modified_files: List[str] = _get_list_modified_files()
    filename: str
    for filename in modified_files:
        elements += _extract_list_modified_elements_from_file(filename)

    return elements


def _extract_list_modified_elements_from_file(filename: str) -> List[str]:
    """
    Return a list of all the methods modified during the last commit in a given file.

    :param filename: File to inspect.
    :type filename: str
    :return: List of modified element names
    :rtype: List[str]
    """
    modified_elements: List[str] = []
    if not os.path.exists(filename):
        return modified_elements

    element_list: List[Element] = code_analyzer.get_list_elements(filename=filename)

    last_commit_sha: str = _get_last_commit_sha()

    element: Element
    for element in element_list:
        last_update_commit_sha: str = _get_last_update_commit_sha(element)

        # If the element has been modified during the last commit
        if last_commit_sha == last_update_commit_sha:
            modified_elements.append(str(element))

    return modified_elements


def _get_last_commit_sha() -> str:
    """
    Return the last commit SHA.

    :return: Last commit SHA.
    :rtype: str
    """
    repo: git.Repository = git.Repository('.')
    last_commit: Commit = repo.revparse_single('HEAD')
    last_commit_sha: str = str(last_commit.id)
    return last_commit_sha


def _get_last_update_commit_sha(element: Element) -> str:
    """
    Return the SHA of the commit during which the element has been modified last.

    :param element: The modified element.
    :type element: Element
    :return: Commit SHA of the last element modification.
    :rtype: str
    """
    hidden_filename: str = '.git_log'
    last_update_commit_sha: str = ''

    filename: str = element.filename
    funcname: str = element.funcname

    # regex: str = element.get_regex()
    # run(f'git log -s -L {regex}:{filename} > {hidden_filename}')

    run(f'git log -s -L :{funcname}:{filename} > {hidden_filename}')
    with io.open(hidden_filename, 'r') as file:
        first_line: str = file.readline()
        if first_line:
            # Get SHA of the last commit
            last_update_commit_sha = first_line.split(' ')[1]
            # remove the \n character
            last_update_commit_sha = last_update_commit_sha.strip()

    run(f'{commands["remove"]} {hidden_filename}')

    return last_update_commit_sha
