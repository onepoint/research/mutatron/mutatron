# Ligne nécessaire pour utiliser le typing python
from __future__ import annotations

import io
from typing import List, Dict, TYPE_CHECKING, Optional

from javalang.tree import MethodDeclaration

from src.code_analysis import code_analyzer
from src.code_analysis.Element import Element
from src.mutation_testing.operators import list_operators
from src.mutation_testing.operators.Operator import Operator

# TYPE_CHECKING est toujours False, nécessaire pour utiliser le typing python
# en évitant les imports circulaires
if TYPE_CHECKING:
    from src.mutation_testing.mutations.Mutation import Mutation


def _extract_mutations(filename: str) -> Dict[str, List[Mutation]]:
    """
    Get all the possible mutations for a given file

    :param filename: The path to the file to extract mutations from
    :type filename: str
    :return: A dictionnary with each key being the element names associated to list of mutations to apply.
    :rtype: Dict[str, List[Mutation]]
    """
    mutations_per_element: Dict[str, List[Mutation]] = {}
    nodes: List[MethodDeclaration] = code_analyzer.get_list_element_nodes(filename)

    operator: Operator
    for operator in list_operators:
        node: MethodDeclaration
        for node in nodes:
            mutations: List[Mutation] = operator.generate_mutations(node)
            if mutations:
                element: Element = code_analyzer.extract_element_from_node(node, filename)
                element_name: str = str(element)
                if element_name not in mutations_per_element:
                    mutations_per_element[element_name] = []
                previous_mutations: List[Mutation] = mutations_per_element[element_name]
                mutations_per_element[str(element)] = [*previous_mutations, *mutations]

    return mutations_per_element


def mutate_elements(filename: str, element_names: Optional[List[str]] = None) -> List[str]:
    """
    Entry point of the mutagen module. Apply all available mutations to a given file, and retrieve the list of those mutations.

    :param element_names: The element names to mutate
    :type element_names:
    :param filename: The file to mutate.
    :type filename: str
    :return: A boolean specifiying if the file has been actually been modified
    :rtype: bool
    """
    mutated_elements: List[str] = []
    mutations: Dict[str, List[Mutation]] = _extract_mutations(filename)

    if mutations:
        # If no name element is provided, mutate all the file
        if element_names is None:
            element_names = list(mutations.keys())

        file_lines: List[str] = io.open(filename).read().split('\n')

        for element_name in element_names:
            if element_name in mutations:
                file_lines = _mutate_lines(file_lines, mutations[element_name])
                mutated_elements.append(element_name)

        with io.open(filename, 'w') as file:
            file.write('\n'.join(file_lines))

    return mutated_elements


def _mutate_lines(original_file_lines: List[str], mutations: List[Mutation]) -> List[str]:
    """
    Mutate a set of string lines representing a file content.

    :param original_file_lines: List of original lines to mutate.
    :type original_file_lines: List[str]
    :param mutations: List of mutations to apply to the lines.
    :type mutations: List[Mutation]
    :return: The mutated lines.
    :rtype: List[str]
    """
    for mutation in mutations:
        line_number = mutation.line_number
        # line_number-1 => required because line numbers start at 1 instead of 0
        original_file_lines[line_number - 1] = mutation.apply(line=original_file_lines[line_number - 1])

    return original_file_lines
