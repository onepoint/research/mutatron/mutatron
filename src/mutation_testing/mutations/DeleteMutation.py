from __future__ import annotations

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation


class DeleteMutation(ReplaceMutation):
    def __init__(self, line_number: int, to_delete: str):
        super().__init__(
            line_number=line_number,
            to_replace=to_delete,
            replacement=''
        )
