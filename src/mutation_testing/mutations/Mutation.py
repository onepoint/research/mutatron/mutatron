from __future__ import annotations

from abc import abstractmethod


class Mutation:
    def __init__(self, line_number: int):
        self.line_number: int = line_number

    @abstractmethod
    def apply(self, line: str) -> str: pass