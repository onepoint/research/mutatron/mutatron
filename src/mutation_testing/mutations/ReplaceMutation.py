from __future__ import annotations

import re

from src.mutation_testing.mutations.Mutation import Mutation


class ReplaceMutation(Mutation):
    def __init__(self, line_number: int, to_replace: str, replacement: str):
        super().__init__(line_number)
        self.to_replace: str = to_replace
        self.replacement: str = replacement

    def apply(self, line: str) -> str:
        line_returned: str = line
        if re.search(string=line, pattern=self.to_replace):
            line_returned = re.sub(
                string=line,
                pattern=self.to_replace,
                repl=self.replacement,
                count=1
            )
        return line_returned

    def __str__(self) -> str:
        return f'ReplaceMutation l{self.line_number}: {self.to_replace} -> {self.replacement}'
