from __future__ import annotations

import random
import re
from typing import Optional

import javalang

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class AccessModifierChangeOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.MethodDeclaration)

    def _generate_mutation(self, element: javalang.tree.MethodDeclaration) -> Optional[ReplaceMutation]:
        modifiers = list(element.modifiers)

        if not modifiers:
            return None

        modifier = modifiers[int(random.random() * len(modifiers))]

        if not is_mutable(modifier):
            return None

        to_replace = modifier
        replacement = get_replacement_modifier(modifier)

        return ReplaceMutation(
            line_number=element.position.line,
            to_replace=re.escape(to_replace),
            replacement=replacement
        )


replacements = {
    'private': 'public',
    'protected': 'public'
}


def get_replacement_modifier(modifier: str):
    replacement = modifier
    if modifier in replacements:
        replacement = replacements[modifier]
    return replacement


def is_mutable(modifier: str):
    return modifier in replacements
