from __future__ import annotations

import re
from typing import Optional

import javalang
from bidict import bidict

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class ConditionSwitchOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.IfStatement)

    def _generate_mutation(self, element: javalang.tree.IfStatement) -> Optional[ReplaceMutation]:
        condition = element.condition
        condition_type = condition.__class__.__name__

        if condition_type == 'BinaryOperation':
            to_replace = condition.operator
            replacement = get_replacement_operator(to_replace)
        elif 'prefix_operators' in condition.attrs:
            operators = condition.prefix_operators
            if len(operators) == 0:
                operators.append('')
            operator = operators[0]
            to_replace = f' ({operator}'
            replacement = f' ({get_replacement_operator(operator)}'
        else:
            return None

        return ReplaceMutation(
            line_number=element.position.line,
            to_replace=re.escape(to_replace),
            replacement=replacement
        )


replacements = bidict({
    '&&': '||',
    '!=': '==',
    '!': '',
    '>': '<=',
    '<': '>='
})


def get_replacement_operator(operator: str):
    replacement = operator
    if operator in replacements:
        replacement = replacements[operator]
    elif operator in replacements.inverse:
        replacement = replacements.inverse[operator]
    return replacement
