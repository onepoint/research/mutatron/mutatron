from __future__ import annotations

from typing import Optional

import javalang
from bidict import bidict
from random import random

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class ConditionTrueFalseOperator(Operator):
    def __init__(self):
        super().__init__(
            element_type=javalang.tree.IfStatement,
            order=1
        )

    def _generate_mutation(self, element: javalang.tree.IfStatement) -> Optional[ReplaceMutation]:
        return_value: Optional[ReplaceMutation] = None
        if random() > .5:
            to_replace = r"\(.*\)"
            replacement: str = '(true)' if random() > .5 else '(false)'

            return_value = ReplaceMutation(
                line_number=element.position.line,
                to_replace=to_replace,
                replacement=replacement
            )
        return return_value


replacements = bidict({
    '&&': '||',
    '!=': '==',
    '!': '',
    '>': '<=',
    '<': '>='
})


def get_replacement_operator(operator: str):
    replacement = operator
    if operator in replacements:
        replacement = replacements[operator]
    elif operator in replacements.inverse:
        replacement = replacements.inverse[operator]
    return replacement
