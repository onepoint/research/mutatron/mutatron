from __future__ import annotations

import re
from typing import Optional

import javalang
from bidict import bidict

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class MathSwitchOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.BinaryOperation)

    def _generate_mutation(self, element: javalang.tree.BinaryOperation) -> Optional[ReplaceMutation]:
        to_replace = element.operator
        replacement = get_replacement_operator(to_replace)

        return ReplaceMutation(
            line_number=element.position.line,
            to_replace=re.escape(to_replace),
            replacement=replacement
        )


replacements = bidict({
    '+': '-',
    '*': '/',
})


def get_replacement_operator(operator: str):
    replacement = operator
    if operator in replacements:
        replacement = replacements[operator]
    elif operator in replacements.inverse:
        replacement = replacements.inverse[operator]
    return replacement
