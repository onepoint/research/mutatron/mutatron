from __future__ import annotations

from typing import List

import javalang

from src.mutation_testing.mutations.Mutation import Mutation


class Operator:
    def __init__(self, element_type: javalang.ast.Node, order=0):
        self.element_type = element_type
        self.order = order

    def generate_mutations(self, element: javalang.tree.MethodDeclaration) -> List[Mutation]:
        mutable_elements = self._get_mutable_elements(element)
        mutations = [self._generate_mutation(mutable_element) for mutable_element in mutable_elements]
        mutations = [mutation for mutation in mutations if mutation is not None]
        return mutations

    def _get_mutable_elements(self, element: javalang.tree.MethodDeclaration) -> List[javalang.ast.Node]:
        return [node for _, node in element.filter(self.element_type)]

    def _generate_mutation(self, element: javalang.tree.MethodDeclaration) -> Mutation:
        return Mutation(line_number=0)
