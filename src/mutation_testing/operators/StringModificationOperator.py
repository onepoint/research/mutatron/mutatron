from __future__ import annotations

import re
from typing import Optional

import javalang

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class StringModificationOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.Literal)

    def _generate_mutation(self, element: javalang.tree.Literal) -> ReplaceMutation:
        value: str = element.value
        first_char = value[0]
        last_char = value[-1]
        mutation: Optional[ReplaceMutation] = None
        is_a_string: bool = (first_char == '"' or last_char == "'")
        same_limits = first_char == last_char
        is_not_a_single_character = len(value) > 3
        if is_a_string and same_limits and is_not_a_single_character:
            mutation = ReplaceMutation(
                to_replace=re.escape(element.value),
                replacement=f'{first_char}XX{element.value[1:-1]}XX{last_char}',
                line_number=element.position.line
            )

        return mutation
