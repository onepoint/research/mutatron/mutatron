import os
import pathlib
from importlib import import_module
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.mutation_testing.operators.Operator import Operator

list_operators = []

module_folder: pathlib.Path = pathlib.Path(__file__).parent.absolute()
file: str
for file in os.listdir(module_folder):
    if 'Operator' in file and file != 'Operator.py':
        operator_name: str = file[:-3]
        module = import_module(f'src.mutation_testing.operators.{operator_name}')
        operator: Operator = getattr(module, operator_name)()
        list_operators.append(operator)

list_operators.sort(key=lambda op: op.order)
