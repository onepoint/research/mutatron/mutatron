from __future__ import annotations

import random
import re
from typing import Optional

import javalang

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class ReferenceComparisonOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.IfStatement)

    def _generate_mutation(self, element: javalang.tree.LocalVariableDeclaration) -> Optional[ReplaceMutation]:
        condition: Optional[javalang.tree.BinaryOperation] = element.condition
        if condition is None \
                or not hasattr(condition, 'operator') \
                or condition.operator != '==' \
                or condition.__class__.__name__ != 'BinaryOperation' \
                or random.random() < .5:  # Add a random condition to also allow ConditionSwitchOperator
            return None

        try:
            operandr: str = condition.operandr.value
            to_replace = f' == {operandr}'
            replacement = f'.equals({operandr})'
        except Exception as e:
            return None

        return ReplaceMutation(
            line_number=element.position.line,
            to_replace=re.escape(to_replace),
            replacement=replacement
        )
