from __future__ import annotations

import random
import re
from typing import Optional

import javalang

from src.mutation_testing.mutations.ReplaceMutation import ReplaceMutation
from src.mutation_testing.operators.Operator import Operator


class StaticModifierInsertionOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.LocalVariableDeclaration)

    def _generate_mutation(self, element: javalang.tree.LocalVariableDeclaration) -> Optional[ReplaceMutation]:
        var_type: str = ''
        for node in element.children:
            if node.__class__.__name__ == 'ReferenceType':
                var_type = node.name

        if not var_type:
            return None

        to_replace = var_type
        replacement = f'static {var_type}'

        return ReplaceMutation(
            line_number=element.position.line,
            to_replace=re.escape(to_replace),
            replacement=replacement
        )
