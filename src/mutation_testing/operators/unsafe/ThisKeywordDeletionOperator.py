from __future__ import annotations

from typing import Optional

import javalang

from src.mutation_testing.mutations.DeleteMutation import DeleteMutation
from src.mutation_testing.operators.Operator import Operator


class ThisKeywordDeletionOperator(Operator):
    def __init__(self):
        super().__init__(javalang.tree.StatementExpression)

    def _generate_mutation(self, element: javalang.tree.StatementExpression) -> Optional[DeleteMutation]:
        mutation: Optional[DeleteMutation] = None
        line: int = 0
        to_delete: str = ''

        for child in element.children:
            if type(child) == javalang.tree.Assignment and type(child.expressionl) == javalang.tree.This:
                line = element.position.line
                to_delete = 'this.'
                break

        if line and to_delete:
            mutation = DeleteMutation(
                line_number=element.position.line,
                to_delete=to_delete
            )

        return mutation
