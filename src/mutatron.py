from typing import List, Optional

import numpy as np

import config
from src.os_commands import mutable_print
from src.repository.connectors.DataConnector import DataConnector


class Mutatron:
    def __init__(self, data_connector: DataConnector):
        """
        Retrieve all informations in database and save them in an object ready to reorder tests.
        """
        mutable_print('Get data from DB')
        self.test_element_fails: np.array = data_connector.get_test_element_fails()
        self.element_counters: np.array = data_connector.get_element_counters()
        self.test_fails: np.array = data_connector.get_test_fails()
        self.test_counters: np.array = data_connector.get_test_counters()
        self.test_durations: np.array = data_connector.get_test_durations()

        self.n_tests: int = self.test_counters.size
        self.n_elements: int = self.element_counters.size

        self.p_ti: Optional[np.array] = None
        self.p_ti_ek: Optional[np.array] = None
        self.beta: Optional[np.array] = None
        self.p_ti_with_ek: Optional[np.array] = None
        self.switch_matrix: Optional[np.array] = None

    def _update_prob_matrices(self, index_modified_elements: List[int]) -> None:
        """
        Update all relevant matrices in order to perform further optimization computations.
        Relevant matrices are:
        - P(t_i) = probability for a test i to fail
        - Beta = ratio representing the information given by the modified elements
        - P(t_i | e_k) probability for a test i to fail knowing that elements {k} has been modified

        :param index_modified_elements: List of modified element indices.
        :type index_modified_elements: List[int]
        """
        self._compute_p_ti()
        self._compute_p_ti_ek()
        self._compute_beta()
        self._compute_p_ti_with_ek(index_modified_elements)
        self._compute_switch_matrix()

    def _compute_switch_matrix(self) -> None:
        """
        The switch matrice is born when mutatron was based on bubble sort. To know if two adjacents elements should be permuted,
        one should have to look to a specific condition related to ratio between probabilities and ratio between costs.
        switch_matrice[i, j] specifies if tests i and j should be permuted if i is after j.
        Even if mutatron is no longer based on bubble sort, this matrice is used along with a trick to know the best test order.
        """
        p_ti_with_ek: np.array = self.p_ti_with_ek.reshape((self.n_tests, 1))
        costs: np.array = self.test_durations.reshape((self.n_tests, 1))
        cost_proba_matrice: np.array = np.matmul(costs, p_ti_with_ek.T)
        self.switch_matrix = (cost_proba_matrice - cost_proba_matrice.T) > 0

    def _compute_p_ti(self) -> None:
        """
        Computes a priori probabilities for tests to fail.
        """
        mutable_print('-- Compute P(t_i)')
        self.p_ti = self.test_fails / self.test_counters

    def _compute_p_ti_with_ek(self, index_modified_elements: List[int]) -> None:
        """
        Computes a posteriori probabilities for tests to fail knowing which elements have been modified.

        :param index_modified_elements: List of modified element indices.
        :type index_modified_elements: List[int]
        """
        mutable_print('-- Compute P(t_i|e_k)')
        p_ti_with_ek: np.array = self.p_ti * np.prod(self.beta[:, index_modified_elements], axis=1)
        func: callable = np.vectorize(lambda x: min(max(x, 0), 1) * config.P_DEV_ERROR)
        self.p_ti_with_ek = func(p_ti_with_ek)

    def _compute_p_ti_ek(self) -> None:
        """
        Computes beta coefficients, representing for a given test i and element k, the information provided by the fact
        that element k has been modified.
        """
        mutable_print('-- Compute P(t_i|e_k)')

        elements_modified: np.array = np.concatenate([self.element_counters] * self.n_tests)
        elements_modified = elements_modified.reshape(self.n_tests, self.element_counters.size)
        elements_modified[elements_modified == 0] = 1

        self.p_ti_ek: np.array = self.test_element_fails / elements_modified

    def _compute_beta(self) -> None:
        mutable_print('-- Compute betas')
        p_ti_2: np.array = np.concatenate([self.p_ti] * self.n_elements).reshape(self.n_elements, self.n_tests).T
        p_ti_2[p_ti_2 == 0] = config.EPSILON

        beta: np.array = self.p_ti_ek / p_ti_2
        beta[beta < 1] = 1
        self.beta = beta

    def optimize(self, index_modified_elements: List[int]) -> List[int]:
        """
        Entry point of the class to get the best test order knowing a list of modified elements.

        :param index_modified_elements: List of modified element indices.
        :type index_modified_elements: List[int]
        :return: Tests ids ordered using modified elements info.
        :rtype: List[int]
        """
        mutable_print('Optimizing')
        shifted_indices: List[int] = [x - 1 for x in index_modified_elements]
        self._update_prob_matrices(shifted_indices)
        number_switches: np.array = np.sum(self.switch_matrix, axis=1)
        ordering: np.array = number_switches.argsort()
        tests_indices = [int(x + 1) for x in ordering]

        return tests_indices

    def compute_cost_rollout(self, test_order: List[int]) -> float:
        """
        Given a test ordering, compute the expected time to spend before encountering a fail.

        :param test_order: Sequence of test ids to analyze.
        :type test_order: List[int]
        :return: Expected time between test launching and fail encounter.
        :rtype: float
        """
        # Ajoute toutes les actions pondérées
        total_cost: float = 0
        total_proba: float = 1
        action: int
        for action in test_order:
            total_cost += total_proba * self.test_durations[action]
            total_proba *= 1 - self.p_ti_with_ek[action]

        return total_cost

    def compute_original_cost_rollout(self) -> float:
        """
        Wrapper around compute_cost_rollout for the specific case of the original sequence (without ordering).
        :return: Expected time between test launch and fail encounter for tests without ordering.
        :rtype: float
        """
        return self.compute_cost_rollout(list(range(self.n_tests)))

    def get_cumulative_probas(self, list_tests: List[int]) -> List[float]:
        """
        List of probabilities of same size that the `list_test` parameter, where every value is equal to the probability
        of encountering a test fail between test launch and this element. It converges to 1 and is strictly increasing.

        :param list_tests: Test ordering to analyze.
        :type list_tests: List[int]
        :return: List of cumulative probabilities.
        :rtype: List[float]
        """
        probas: List[float] = []
        cumulative_proba: float = 1
        test: int
        for test in list_tests:
            cumulative_proba *= (1 - self.p_ti_with_ek[test])
            probas.append(1 - cumulative_proba)
        return probas

    def get_test_list_with_time_threshold(self, modified_element_ids: List[int], threshold: int = 60):
        test_list: List[int] = self.optimize(modified_element_ids)
        optimized_list = self.set_time_threshold_to_optimized_list(test_list, threshold)
        return optimized_list

    def set_time_threshold_to_optimized_list(self, test_list: List[int], threshold: int = 60):
        shifted_test_list: List[int] = [x - 1 for x in test_list]
        cumulative_times: np.array = np.array(self.get_cumulative_times(shifted_test_list))
        max_index: int = np.sum(cumulative_times <= threshold)
        optimized_list: List[int] = test_list[:max_index]
        return optimized_list

    def get_cumulative_times(self, shifted_test_list: List[int]):
        times = [self.test_durations[i] for i in shifted_test_list]
        cumul: float = 0
        cumulative_times: List[float] = []
        for time in times:
            cumul += time
            cumulative_times.append(cumul)
        return cumulative_times

    def get_test_list_with_confidence_threshold(self, modified_element_ids: List[int], threshold: float = .95) -> List[
        int]:
        """
        Get an optimized test ordering truncated at a specific probability threshold. The list is truncated in such a way that
        there is a [threshold] probability that, if a test error should occurs, it occurs in the given list.
        Default value is .95, meaning the probability threshold is 95%.

        :param modified_element_ids: List of all modified elements.
        :type modified_element_ids: List[int]
        :param threshold: Probability threshold to reach before truncating the test list.
        :type threshold: float
        :return: Truncated test ids ordering.
        :rtype: List[int]
        """
        test_list: List[int] = self.optimize(modified_element_ids)
        optimized_list = self.set_confidence_threshold_to_optimized_list(test_list, threshold)
        return optimized_list

    def set_confidence_threshold_to_optimized_list(self, test_list: List[int], threshold: float = .95):
        shifted_test_list: List[int] = [x - 1 for x in test_list]
        cumulative_probas: np.array = np.array(self.get_cumulative_probas(shifted_test_list))
        max_index: int = np.sum(cumulative_probas <= threshold)
        optimized_list: List[int] = test_list[:max_index]
        return optimized_list

    @staticmethod
    def apfd(list_actions: List[int], results_tests: np.array) -> float:
        """
        Return the Average Percentage of Fault Detected for a given test ordering knowing tests results.

        :param list_actions: Test ordering to analyze.
        :type list_actions: List[int]
        :param results_tests: List of test result, 0 for fail and 1 for success.
        :type results_tests: numpy.array
        :return: The Average Percentage of Fault Detected for the given sequence.
        :rtype: float
        """
        index_tests: np.array = np.where(results_tests == 0)[0]
        if index_tests.size == 0:
            return -1
        index_in_list_actions: np.array = np.array([np.where(list_actions == x)[0][0] for x in index_tests]) + 1
        n: int = results_tests.size
        m: int = index_tests.size
        score: float = 1 - (index_in_list_actions.sum() / (n * m) + 1 / (2 * n))
        return score

    def compute_real_cost(self, test_ordering: List[int], failing_tests: List[int]) -> float:
        """
        Compute the real cost of a test ordering, knowing the list of failing tests.

        :param test_ordering: Test ordering to analyze.
        :type test_ordering: List[int]
        :param failing_tests: List of failing test indices.
        :type failing_tests: List[int]
        :return: Real cost of the run.
        :rtype: float
        """
        total_cost: float = 0
        i: int
        for i in range(len(test_ordering)):
            test: int = test_ordering[i]  # Move from Postgre index to array one
            total_cost += self.test_durations[test - 1]
            if test in failing_tests:
                break
        return total_cost
