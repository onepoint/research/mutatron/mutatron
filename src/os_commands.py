import os
import signal
import subprocess
import sys
from typing import Dict, Any

import config

# When the module is loaded, define the correct set of os dependant commands to use
commands: Dict[str, str] = {}
if sys.platform == 'linux' or sys.platform == 'linux2':
    commands = {
        'remove': 'rm',
        'move': 'mv',
        'copy': 'cp'
    }
elif sys.platform == 'win32':
    commands = {
        'remove': 'del',
        'move': 'move',
        'copy': 'copy'
    }


def run(command, mute=None, timeout=None) -> bool:
    if mute is None:
        mute = config.MUTE_SHELL

    params: Dict[str, Any] = {
        'shell': True,
        'start_new_session': True
    }
    if mute:
        params['stdout'] = subprocess.DEVNULL
        params['stderr'] = subprocess.DEVNULL

    # Base of https://alexandra-zaharia.github.io/posts/kill-subprocess-and-its-children-on-timeout-python/
    succeed: bool = True
    process: subprocess.Popen = subprocess.Popen(command, **params)

    # Waiting for the process to finish if necessary
    try:
        title: str = "Executing"
        if timeout is not None:
            title += f" (timeout {timeout:.2f})"
        if not mute:
            print(f'{title} : {command}')
        process.wait(timeout=timeout)
    except subprocess.TimeoutExpired:
        succeed = False

    try:
        # To ensure that no process remains
        os.killpg(os.getpgid(process.pid), signal.SIGTERM)
    except ProcessLookupError:
        # No group has been created, better ask for forgiveness than permission
        pass

    return succeed


def copy_project(path: str) -> str:
    TARGET: str = config.COPIED_PROJECT_PATH
    run(f'{commands["copy"]} -r {path} {TARGET}')
    gitify(TARGET)


def mutable_print(string):
    if not config.MUTE_PRINT:
        print(string)


def gitify(target: str):
    if not os.path.isdir(os.path.join(target, '.git')):
        current_path: str = os.getcwd()
        os.chdir(target)
        run(f'git init')
        run('git add .')
        run('git commit -m "Initial commit"')
        os.chdir(current_path)
