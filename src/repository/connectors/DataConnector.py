import numpy as np


class DataConnector:
    def get_test_element_fails(self) -> np.array:
        raise Exception("get_test_element_fails n'est pas défini")

    def get_element_counters(self) -> np.array:
        raise Exception("get_element_counters n'est pas défini")

    def get_test_fails(self) -> np.array:
        raise Exception("get_test_fails n'est pas défini")

    def get_test_counters(self) -> np.array:
        raise Exception("get_test_counters n'est pas défini")

    def get_test_durations(self) -> np.array:
        raise Exception("get_test_durations n'est pas défini")
