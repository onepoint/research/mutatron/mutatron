import numpy as np

from src.repository.connectors.DataConnector import DataConnector


class FakeProjectDataConnector(DataConnector):
    def __init__(self, n_tests, n_elements, test_durations):
        self.test_element_fails = np.zeros((n_tests, n_elements))
        self.element_counters = np.zeros(n_elements)
        self.test_fails = np.zeros(n_tests)
        self.test_counters = np.zeros(n_tests)
        self.test_durations = np.array(test_durations)

    def get_test_element_fails(self) -> np.array:
        return self.test_element_fails

    def get_element_counters(self) -> np.array:
        return self.element_counters

    def get_test_fails(self) -> np.array:
        return self.test_fails

    def get_test_counters(self) -> np.array:
        return self.test_counters

    def get_test_durations(self) -> np.array:
        return self.test_durations
