import numpy as np

import src.repository.row_management as rm
from src.repository.connectors.DataConnector import DataConnector


class PostgreDataConnector(DataConnector):
    def get_test_element_fails(self) -> np.array:
        return rm.retrieve_2darray_from_db('test_element_fails')

    def get_element_counters(self) -> np.array:
        return rm.retrieve_1darray_from_db('element_counters', 'n')

    def get_test_fails(self) -> np.array:
        return rm.retrieve_1darray_from_db('test_fails', 'n')

    def get_test_counters(self) -> np.array:
        return rm.retrieve_1darray_from_db('test_counters', 'n')

    def get_test_durations(self) -> np.array:
        return rm.retrieve_1darray_from_db('test_durations', 'duration')
