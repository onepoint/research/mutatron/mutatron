from __future__ import annotations

import logging
from typing import TYPE_CHECKING

import sqlalchemy as alch

import config

if TYPE_CHECKING:
    from sqlalchemy.future import Engine


class EngineSQLA:
    """
    Singleton class to get access to the SQLAlchemy engine
    """
    _instance: Engine = None

    @classmethod
    def get_instance(cls: EngineSQLA) -> Engine:
        """
        Usual instance retrieving method of a singleton.

        :return: The instance of the SQLAlchemy engine
        :rtype: sqlalchemy.future.Engine
        """
        if cls._instance is None:
            username: str = config.POSTGRE_MASTER_USERNAME
            password: str = config.POSTGRE_MASTER_PASSWORD
            address: str = config.POSTGRE_HOST
            database: str = 'mutatron_db'
            EngineSQLA._instance = alch.create_engine(f"postgresql://{username}:{password}@{address}/{database}")

        return cls._instance


def execute_stmt(stmt: str) -> alch.engine.CursorResult:
    """
    Execute a SQL statement directly through a connection to the database.

    :param stmt: SQL statement to execute.
    :type stmt: str
    :return: The execution result.
    :rtype: sqlalchemy.engine.CursorResult
    """
    with EngineSQLA.get_instance().begin() as conn:
        return conn.execute(stmt)


def check_postgre_usable(postgre_string: str) -> bool:
    """
    Check if everything is fine before launching mutatron.

    :return: Is the installation of postgre OK.
    :rtype: bool
    """
    valid: bool = True
    try:
        engine: Engine = alch.create_engine(postgre_string)
        engine.connect()
    except Exception as exc:
        valid = False
        logging.error(exc)
    return valid
