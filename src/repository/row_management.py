from __future__ import annotations

from typing import TYPE_CHECKING, List, Optional, Dict, Union, Any
import warnings

import numpy as np
import sqlalchemy as alch
from sqlalchemy import orm
from tqdm import tqdm

from .engine import EngineSQLA, execute_stmt
from .table_management import get_table
from .. import types

if TYPE_CHECKING:
    from sqlalchemy.future import Engine
    from sqlalchemy import Table


def _initialize_table_with_list(table_name: str, list_values: List[Optional[str]]) -> None:
    """
    Initialize a table based on a list of values. Values are string for which we need an int to get a valid ID,
    or for which we will count things, in this case `list_values` is a list of `None`.

    :param table_name: Table to initialize
    :type table_name: str
    :param list_values: List of values to insert
    :type list_values: List[Optional[str]]
    """
    table: Table = get_table(table_name)

    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        conn.execute(
            alch.insert(table),
            [{"name": name} for name in list_values]
        )


def _initialize_empty_rows(table_name: str, n: int):
    """
    Small wrapper around `initialize_table_with_list` with list of `None` values.

    :param table_name: Table to initialize
    :type table_name: str
    :param n: Number of empty values to insert
    :type n: int
    """
    _initialize_table_with_list(table_name, list_values=[None] * n)


def _initialize_empty_cross_rows(table_name: str, n_tests: int, n_elements: int):
    """
    Initialize a table responsible for counting cross occurences of elements and tests.

    :param table_name: Table to initialize
    :type table_name: str
    :param n_tests: Number of tests
    :type n_tests: int
    :param n_elements: Number of elements
    :type n_elements: int
    """
    table: Table = get_table(table_name)

    engine: Engine = EngineSQLA.get_instance()
    with engine.connect() as conn:
        i: int
        for i in tqdm(range(n_tests), desc="Tests initialized"):
            conn.execute(
                alch.insert(table),
                [{'test_id': i + 1, 'element_id': x + 1} for x in range(n_elements)]
            )


def update_empty_tables_with_names(test_names: List[str], element_names: List[str]) -> None:
    """
    General method to update all tables based on a list of test and element names.

    :param test_names: List of test names
    :type test_names: List[str]
    :param element_names: List of element names
    :type element_names: List[str]
    """
    n_tests: int = len(test_names)
    n_elements: int = len(element_names)

    _initialize_table_with_list('test_names', test_names)
    _initialize_table_with_list('element_names', element_names)
    _initialize_empty_rows('test_counters', n=n_tests)
    _initialize_empty_rows('element_counters', n=n_elements)
    _initialize_empty_rows('test_fails', n=n_tests)
    _initialize_empty_rows('test_durations', n=n_tests)
    _initialize_empty_cross_rows('test_element_fails', n_tests=n_tests, n_elements=n_elements)


def _get_item_ids(item_type: str, item_names: List[str]) -> Dict[str, int]:
    """
    Retrieve a dictionnary of {item name: item id}. Required to translate an item name to its usable id.
    An item can either be a test or a code element.

    :param item_type: Type of item, can be either 'test' or 'element'
    :type item_type: str
    :param item_names: List of item names
    :type item_names: List[str]
    :return: Dictionnary with the id corresponding to each item name
    :rtype: Dict[str, int]
    """
    table_name: str = f'{item_type}_names'
    table: Table = get_table(table_name)
    engine: Engine = EngineSQLA.get_instance()
    with orm.Session(engine) as session:
        result = session.query(table).filter(table.columns['name'].in_(item_names))
        item_ids = {
            row[1]: row[0]
            for row in result
        }

        not_exist = [name for name in item_names if name not in item_ids]
        if len(not_exist) > 0:
            warnings.warn(f'Wrong {item_type} name : {not_exist} does not exist in database')

    return item_ids


def get_test_ids(test_names: List[str]) -> Dict[str, int]:
    """
    Wrapper around `_get_item_ids` for the tests.

    :param test_names: List of item names
    :type test_names: List[str]
    :return: Dictionnary with the id corresponding to each test name
    :rtype: Dict[str, int]
    """
    return _get_item_ids('test', test_names)


def get_element_ids(element_names: List[str]) -> Dict[str, int]:
    """
    Wrapper around `_get_item_ids` for the code elements.

    :param element_names: List of item names
    :type element_names: List[str]
    :return: Dictionnary with the id corresponding to each test name
    :rtype: Dict[str, int]
    """
    element_ids: Dict[str, int] = {} if not element_names else _get_item_ids('element', element_names)
    return element_ids


def _increment_table_using_ids(target_table: str, ids_to_update: List[int]) -> None:
    """
    Update a test related table, incrementing each of the provided test n value by 1.

    :param target_table: The table to update
    :type target_table: str
    :param ids_to_update: IDs to update
    :type ids_to_update: List[int]
    """
    table = get_table(target_table)
    stmt: alch.sql.expression.Update = (
        alch.update(table).
            where(table.c.id.in_(ids_to_update)).
            values(n=table.c.n + 1)
    )
    execute_stmt(stmt)


def _update_test_fails(failed_test_ids: List[int]) -> None:
    """
    Wrapper around _update_test_table for the specific case of the test_fails table

    :param failed_test_ids: Test ids to update
    :type failed_test_ids: List[int]
    """
    _increment_table_using_ids('test_fails', failed_test_ids)


def _update_element_counters(modified_elements_id: List[int]) -> None:
    """
    Wrapper around _update_test_table for the specific case of the test_counters table

    :param modified_elements_id: Element ids to update
    :type modified_elements_id: List[int]
    """
    _increment_table_using_ids('element_counters', modified_elements_id)


def _update_test_counters(executed_test_ids: List[int]) -> None:
    """
    Wrapper around _update_test_table for the specific case of the test_counters table

    :param executed_test_ids: Test ids to update
    :type executed_test_ids: List[int]
    """
    _increment_table_using_ids('test_counters', executed_test_ids)


def _update_test_element_fails(failed_test_ids: List[int], modified_element_ids: List[int]) -> None:
    """
    Update the test_element_fails table, incrementing each duo between failed_test_ids and modified_element_ids by 1.

    :param failed_test_ids: Failed test IDs
    :type failed_test_ids: List[int]
    :param modified_element_ids: Modified element IDs
    :type modified_element_ids: List[int]
    """
    target_table: alch.Table = get_table('test_element_fails')
    stmt: alch.sql.expression.Update = (
        alch.update(target_table).
            where(target_table.c.test_id.in_(failed_test_ids), target_table.c.element_id.in_(modified_element_ids)).
            values(n=target_table.c.n + 1)
    )
    execute_stmt(stmt)


def update_test_durations(test_results: List[types.TestInfos]):
    """
    Update mean test durations in database using last test results.

    :param test_results: Infos about last test run.
    :type test_results: List[TestInfos]
    """
    target_table: alch.Table = get_table('test_durations')
    test_ids = get_test_ids([item['name'] for item in test_results])
    test_results_dict: Dict[str, Union[float, int]] = {
        test_ids[item['name']]: item['duration']
        for item in test_results
    }

    results = execute_stmt(
        alch.select(target_table).
            where(target_table.c.id.in_(test_ids.values()))
    )

    for item in tqdm(results):
        test_id = item[0]
        new_duration = (item[1] * item[2] + test_results_dict[item[0]]) / (item[2] + 1) + 0.001
        new_run_count = item[2] + 1
        _update_test_duration_row(test_id, new_duration, new_run_count)


def _update_test_duration_row(test_id: int, new_duration: float, new_run_count: int):
    """
    Update a single test duration.

    :param new_duration: Duration new value
    :type new_duration: float
    :param new_run_count: Run count new value
    :type new_run_count: int
    :param test_id: Test id
    :type test_id: int
    """
    target_table: alch.Table = get_table('test_durations')
    stmt = (
        alch.update(target_table).
            where(target_table.c.id == test_id).
            values(
            id=test_id,
            duration=new_duration,
            run_count=new_run_count
        )
    )
    execute_stmt(stmt)


def update_table_with_test_results(modified_elements_names: List[str], test_results: List[types.TestInfos]):
    """
    Update all related table using a mutation results

    :param modified_elements_names: List of all modified elements
    :type modified_elements_names: List[str]
    :param test_results: List of small dictionnaries containing a test name and its result
    :type test_results: List[Dict[str, str]]
    """
    # Test names
    test_names: List[str] = [test['name'] for test in test_results]
    failed_test_names: List[str] = [test['name'] for test in test_results if test['result'] != 'success']

    # IDs dictionnaries
    test_ids: Dict[str, int] = get_test_ids(test_names)
    element_ids: Dict[str, int] = get_element_ids(modified_elements_names)

    # Retrieve item IDs
    failed_test_ids: List[int] = [test_ids[name] for name in failed_test_names if name in test_ids]
    test_executed_ids: List[int] = [test_ids[name] for name in test_names if name in test_ids]
    modified_element_ids: List[int] = [element_ids[name] for name in modified_elements_names if name in element_ids]

    # Test have failed and elements have been modified
    if failed_test_ids:
        _update_test_fails(failed_test_ids)
        if modified_elements_names:
            _update_test_element_fails(failed_test_ids, modified_element_ids)
            _update_element_counters(modified_element_ids)

    # Test have been run
    if test_results:
        _update_test_counters(test_executed_ids)

    # Whatever the test results, update their durations
    # Remove for the proto, take to much time and the first duration is probably good enough
    # update_test_durations(test_results)


def retrieve_1darray_from_db(table_name: str, target_attr: str) -> np.array:
    """
    Retrieve all the values of the target attribute from the target table as a numpy array.

    :param table_name: Target table name.
    :type table_name: str
    :param target_attr: Target attribute name.
    :type target_attr: str
    :return: All the target attribute values in target table.
    :rtype: numpy.array
    """
    target_table: alch.Table = get_table(table_name)
    results: alch.engine.CursorResult = execute_stmt(
        alch.select(getattr(target_table.c, target_attr))
            .order_by(target_table.c.id.asc())
    )
    results_list: List[Any] = [row[0] for row in results]
    return np.array(results_list)


def retrieve_2darray_from_db(table_name: str) -> np.array:
    """
    Retrieve all the values in the target table as a numpy array.

    :param table_name: Target table name.
    :type table_name: str
    :return: All the target attribute values in target table.
    :rtype: numpy.array
    """
    target_table: alch.Table = get_table(table_name)
    results: alch.engine.CursorResult = execute_stmt(alch.select(target_table))
    results_list: List[Any] = [row for row in results]

    n_tests: int = np.max([item[0] for item in results_list])
    n_elements: int = np.max([item[1] for item in results_list])

    ndarray: np.array = np.zeros((n_tests, n_elements,))
    for (x, y, value) in results_list:
        ndarray[x - 1][y - 1] = value

    return ndarray


def _test_ids_to_names(test_ids: List[int]) -> Dict[int, str]:
    """
    Given a list of test ids, retrieve the corresponding test names.

    :param test_ids: List of test ids to convert in test names.
    :type test_ids: List[int]
    :return: Dictionnary with test ids as key and test names as values.
    :rtype: Dict[int, str]
    """
    table_name: str = f'test_names'
    table: Table = get_table(table_name)
    engine: Engine = EngineSQLA.get_instance()
    with orm.Session(engine) as session:
        result = session.query(table).filter(table.columns['id'].in_(test_ids))
        test_names: Dict[int, str] = {
            row[0]: row[1]
            for row in result
        }
        if len(test_names) != len(test_ids):
            if len(test_names) > len(test_ids):
                not_exist: List[int] = [name for name in test_names if name not in test_names]
                raise Exception(f'Wrong test id : {not_exist} does not exist in database')
            else:
                raise Exception(f'IDs not in base : {len(test_ids)} > {len(test_names)}')
        return test_names


def get_dtest_string(test_ids: List[int]) -> str:
    """
    Based on a list of test ids, return the regex to be used as the maven Dtest parameter.

    :param test_ids: List of test ids to run.
    :type test_ids: List[int]
    :return: The regex for the Dtest parameter.
    :rtype: str
    """
    dtest_params: Dict[str, List[str]] = _get_dtest_params_dict(test_ids)
    dtest_string: str = _get_dtest_string_from_dict(dtest_params)
    return dtest_string


def _get_dtest_params_dict(test_ids: List[int]) -> Dict[str, List[str]]:
    """
    Get a dictionnary of test class names along with the corresponding test names based on a list of test ids.
    This function is used as a first step toward the correct string definition required by the -Dtest parameter
    of maven to run multiple targeted test.
    More info here : https://maven.apache.org/surefire/maven-surefire-plugin/examples/single-test.html

    :param test_ids: Test ids to get names from.
    :type test_ids: List[int]
    :return: Dictionnary with test class names as key and list of test names as values.
    :rtype: Dict[str, List[str]]
    """
    test_names: Dict[int, str] = _test_ids_to_names(test_ids)

    dtest_params: Dict[str, List[str]] = {}
    test_path: str
    for test_path in test_names.values():
        items: List[str] = test_path.split('.')
        test_name: str = items[-1]
        test_class_name: str = items[-2]
        if test_class_name not in dtest_params:
            dtest_params[test_class_name] = []
        dtest_params[test_class_name].append(test_name)

    return dtest_params


def _get_dtest_string_from_dict(dtest_params: Dict[str, List[str]]) -> str:
    """
    Based on a test class name to list of test names dictionnary, produces the string required by the Dtest parameter
    of maven.
    More info here : https://maven.apache.org/surefire/maven-surefire-plugin/examples/single-test.html

    :param dtest_params: Dictionnary of test class names and list of test names.
    :type dtest_params: Dict[str, List[str]]
    :return: The correctly formatted string required by Dtest.
    :rtype: str
    """
    dtest_string: str = ""
    for test_class_name, test_list in dtest_params.items():
        if dtest_string != "":
            dtest_string += ", "
        dtest_string += f"{test_class_name}#{'+'.join(test_list)}"
    return dtest_string


def _update_table_with_new_items(table_name: str, item_ids: List[int]) -> None:
    """
    If a new item not already in tables in found, update the given table name with the required lines.

    :param table_name: Table name to update.
    :type table_name: str
    :param item_ids: List of new items to add
    :type item_ids: List[int]
    """
    table: Table = get_table(table_name)

    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        # Insert new names
        conn.execute(
            alch.insert(table),
            [{"id": item_id} for item_id in item_ids]
        )


def _update_name_table_for_new_items(table_name: str, item_names: List[str]) -> List[int]:
    """
    When new items (element or test) are discovered, update the id table and return the ids of the newly created items.

    :param table_name: The table to update.
    :type table_name: str
    :param item_names: List of item names.
    :type item_names: List[str]
    :return: List of the corresponding newly created item ids.
    :rtype: List[int]
    """
    table: Table = get_table(table_name)

    # Retrieving existing and new names
    existing_names: List[str] = _get_existing_field_values(table_name, 'name')
    new_names: List[str] = [name for name in item_names if name not in existing_names]

    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        new_ids: List[int] = []
        if new_names:
            # Insert new names
            conn.execute(
                alch.insert(table),
                [{"name": name} for name in new_names]
            )

            # Retrieve new ids
            results: alch.engine.CursorResult = conn.execute(
                alch.select([table.c.id]).
                    where(table.c.name.in_(new_names))
            )
            new_ids = [row[0] for row in results]

    return new_ids


def _update_test_element_fails_with_new_ones(new_test_ids: List[int], new_element_ids: List[int]) -> None:
    """
    Update the test_element_fails table with newly created test and element ids.

    :param new_test_ids: List of newly created test ids.
    :type new_test_ids: List[int]
    :param new_element_ids: List of newly created element ids
    :type new_element_ids: List[int]
    """
    table: Table = get_table('test_element_fails')
    new_pairs: List[Dict[str, int]] = generate_new_test_element_ids_pairs(new_test_ids, new_element_ids)

    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        conn.execute(alch.insert(table), new_pairs)


def get_existing_field_values_without_new_ones(
        table_name: str,
        field_name: str,
        new_item_values: List[Union[int, str]] = None) -> List[Union[int, str]]:
    """
    Get existing ids in the specified table. If new_item_ids is provided, it ensures that ids in the return list
    are not in the new_item_ids one.

    :param table_name: Table name to investigate, either 'element_names' or 'test_names'.
    :type table_name: str
    :param field_name:
    :type field_name:
    :param new_item_values: Optional list of new values to remove from the existing ones.
    :type new_item_values: List[Union[int, str]]
    :return: List of existing values.
    :rtype: List[Union[int, str]]
    """
    if new_item_values is None:
        new_item_values = []

    item_values: List[Union[str, int]] = _get_existing_field_values(table_name, field_name)
    item_values = [item_value for item_value in item_values if item_value not in new_item_values]
    return item_values


def _get_existing_field_values(table_name: str, field_name: str) -> List[Any]:
    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        item_table: Table = get_table(table_name)
        results: alch.engine.CursorResult = conn.execute(alch.select([getattr(item_table.c, field_name)]))
        field_values: List[Any] = [row[0] for row in results]
    return field_values


def generate_new_test_element_ids_pairs(new_test_ids, new_element_ids) -> List[Dict[str, int]]:
    """
    Generate all the test/element ids pairs based based on the newly added ones.

    :param new_element_ids: List of new test ids to add in table.
    :type new_element_ids: List[int]
    :param new_test_ids: List of new element ids to add in table.
    :type new_test_ids: List[int]
    :return: All newly generated test/element ids pairs.
    :rtype: List[Dict[str, int]]
    """
    test_ids: List[int] = get_existing_field_values_without_new_ones('test_names', 'id', new_test_ids)
    element_ids: List[int] = get_existing_field_values_without_new_ones('element_names', 'id', new_element_ids)
    new_pairs: List[Dict[str, int]] = []
    new_pairs += _generate_pairs(test_ids, new_element_ids)
    new_pairs += _generate_pairs(new_test_ids, element_ids)
    new_pairs += _generate_pairs(new_test_ids, new_element_ids)
    return new_pairs


def _generate_pairs(test_ids: List[int], element_ids: List[int]) -> List[Dict[str, int]]:
    """
    Generate all pairs of test/element ids based on the provided lists.

    :param test_ids: List of test ids.
    :type test_ids: List[int]
    :param element_ids: List of element ids.
    :type element_ids: List[int]
    :return: List of all test/element ids pairs.
    :rtype: List[Dict[str, int]]
    """
    return [
        {
            "test_id": test_id,
            "element_id": element_id
        }
        for test_id in test_ids
        for element_id in element_ids
    ]


def _retrieve_useless_item_ids(table_name: str, item_names: List[str]) -> List[int]:
    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        item_table: Table = get_table(table_name)
        results: alch.engine.CursorResult = conn.execute(
            alch.select([item_table.c.id])
                .where(item_table.c.name.not_in(item_names)))
        useless_ids: List[int] = [row[0] for row in results]
    return useless_ids


def _delete_items(table_name: str, field_name: str, delete_targets: List[int]):
    engine: Engine = EngineSQLA.get_instance()
    with engine.begin() as conn:
        item_table: Table = get_table(table_name)
        column = getattr(item_table.c, field_name)
        alch.engine.CursorResult = conn.execute(
            alch.delete(item_table)
                .where(column.in_(delete_targets))
        )


def _remove_useless_items(table_name: str, useless_item_ids: List[int]) -> None:
    _delete_items(
        table_name=table_name,
        field_name='id',
        delete_targets=useless_item_ids
    )


def _remove_useless_test_element_pairs(useless_test_ids: List[int], useless_element_ids: List[int]):
    _delete_items(
        table_name='test_element_fails',
        field_name='test_id',
        delete_targets=useless_test_ids
    )
    _delete_items(
        table_name='test_element_fails',
        field_name='element_id',
        delete_targets=useless_element_ids
    )


def _remove_useless_items_from_name_tables(useless_test_ids: List[int], useless_element_ids: List[int]):
    if useless_test_ids:
        _delete_items(
            table_name='test_names',
            field_name='id',
            delete_targets=useless_test_ids
        )
    if useless_element_ids:
        _delete_items(
            table_name='element_names',
            field_name='id',
            delete_targets=useless_element_ids
        )


def _udpate_tables_with_useless_tests_and_elements(
        useless_test_ids: List[int],
        useless_element_ids: List[int]) -> None:
    if useless_test_ids:
        table_names: List[str] = [
            'test_counters',
            'test_durations',
            'test_fails'
        ]
        for table_name in table_names:
            _remove_useless_items(table_name, useless_test_ids)

    if useless_element_ids:
        _remove_useless_items('element_counters', useless_element_ids)

    if useless_test_ids or useless_element_ids:
        _remove_useless_test_element_pairs(useless_test_ids, useless_element_ids)

    _remove_useless_items_from_name_tables(
        useless_test_ids,
        useless_element_ids
    )


def update_tables(current_test_names: List[str], current_element_names: List[str]):
    new_test_ids: List[int] = _update_name_table_for_new_items('test_names', current_test_names)
    new_element_ids: List[int] = _update_name_table_for_new_items('element_names', current_element_names)
    useless_test_ids: List[int] = _retrieve_useless_item_ids('test_names', current_test_names)
    useless_element_ids: List[int] = _retrieve_useless_item_ids('element_names', current_element_names)

    new_elements: int = len(new_element_ids)
    new_tests: int = len(new_test_ids)
    useless_elements: int = len(useless_element_ids)
    useless_tests: int = len(useless_test_ids)

    print(f'Elements: {new_elements} new / {useless_elements} useless')
    print(f'Tests: {new_tests} new / {useless_tests} useless')

    if new_elements or new_tests:
        print('Updating tables with new items')
        _update_tables_with_new_tests_and_elements(
            new_test_ids=new_test_ids,
            new_element_ids=new_element_ids
        )

    if useless_elements or useless_tests:
        print('Updating tables with useless items')
        _udpate_tables_with_useless_tests_and_elements(
            useless_test_ids=useless_test_ids,
            useless_element_ids=useless_element_ids
        )


def _update_tables_with_new_tests_and_elements(
        new_test_ids: Optional[List[int]] = None,
        new_element_ids: Optional[List[int]] = None):
    """
    Entry point to generate everything required in database when detecting new tests and elements.
    Generate the required ids and every required counter based on test and elements newly detected names.

    :param new_test_ids: List of newly detected tests.
    :type new_test_ids: List[int]
    :param new_element_ids: List of newly detected elements.
    :type new_element_ids: List[int]
    """
    if new_test_ids:
        table_names: List[str] = [
            'test_counters',
            'test_durations',
            'test_fails'
        ]
        for table_name in table_names:
            _update_table_with_new_items(table_name, new_test_ids)

    if new_element_ids:
        _update_table_with_new_items('element_counters', new_element_ids)

    if new_test_ids or new_element_ids:
        _update_test_element_fails_with_new_ones(new_test_ids, new_element_ids)


def get_total_test_time() -> float:
    times = get_test_durations()
    return sum(times)


def get_test_durations() -> List[float]:
    times: List[float] = _get_existing_field_values('test_durations', 'duration')
    return times
