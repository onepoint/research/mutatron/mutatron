from __future__ import annotations

from typing import TYPE_CHECKING

import sqlalchemy as alch
from sqlalchemy.ext.compiler import compiles

from .engine import EngineSQLA

if TYPE_CHECKING:
    from sqlalchemy.future import Engine
    from sqlalchemy import Table
    from sqlalchemy.sql.expression import ClauseElement


def _create_test_element_fails_table(meta: alch.MetaData):
    """
    Create the test_element_fails table is PostgreSQL

    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'test_element_fails', meta,
        alch.Column('test_id', alch.Integer, alch.ForeignKey("test_names.id"), primary_key=True, nullable=False),
        alch.Column('element_id', alch.Integer, alch.ForeignKey("element_names.id"), primary_key=True, nullable=False),
        alch.Column('n', alch.Integer, server_default=alch.text('0'))
    )


def _create_test_fails_table(meta: alch.MetaData):
    """
    Create the test_fails table is PostgreSQL
    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'test_fails', meta,
        alch.Column(
            'id',
            alch.Integer,
            alch.ForeignKey("test_names.id"),
            primary_key=True,
            autoincrement=True
        ),
        alch.Column('n', alch.Integer, server_default=alch.text('0'))
    )


def _create_test_counters_table(meta: alch.MetaData):
    """
    Create the test_counters table is PostgreSQL

    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'test_counters', meta,
        alch.Column(
            'id',
            alch.Integer,
            alch.ForeignKey("test_names.id"),
            primary_key=True,
            autoincrement=True
        ),
        alch.Column('n', alch.Integer, server_default=alch.text('0'))
    )


def _create_test_names_table(meta: alch.MetaData):
    """
    Create the test_names table is PostgreSQL

    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'test_names', meta,
        alch.Column('id', alch.Integer, primary_key=True, autoincrement=True),
        alch.Column('name', alch.String, nullable=False)
    )


def _create_element_names_table(meta: alch.MetaData):
    """
    Create the element_names table is PostgreSQL

    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'element_names', meta,
        alch.Column('id', alch.Integer, primary_key=True, autoincrement=True),
        alch.Column('name', alch.String, nullable=False)
    )


def _create_test_durations_table(meta: alch.MetaData):
    """
    Create the test_durations table is PostgreSQL

    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'test_durations', meta,
        alch.Column('id', alch.Integer, alch.ForeignKey("test_names.id"), primary_key=True, autoincrement=True),
        alch.Column('duration', alch.Float, server_default=alch.text('0')),
        alch.Column('run_count', alch.Integer, server_default=alch.text('0'))
    )


def _create_element_counters_table(meta: alch.MetaData):
    """
    Create the test_counters table is PostgreSQL

    :param meta: The metadata required by SQLAlchemy to create tables
    :type meta: sqlalchemy.MetaData
    """
    alch.Table(
        'element_counters', meta,
        alch.Column(
            'id',
            alch.Integer,
            alch.ForeignKey("element_names.id"),
            primary_key=True,
            autoincrement=True
        ),
        alch.Column('n', alch.Integer, server_default=alch.text('0'))
    )


def create_required_tables() -> None:
    """
    Create the 5 tables required by mutatron to store test results.
    """
    engine: Engine = EngineSQLA.get_instance()
    meta: alch.MetaData = alch.MetaData()

    _create_test_names_table(meta)
    _create_element_names_table(meta)
    _create_test_fails_table(meta)
    _create_test_counters_table(meta)
    _create_test_element_fails_table(meta)
    _create_element_counters_table(meta)
    _create_test_durations_table(meta)

    meta.create_all(engine)


@compiles(alch.schema.DropTable, "postgresql")
def __compile_drop_table(element: ClauseElement, compiler, **kwargs) -> str:
    """
    SQLAlchemy way to implement the "CASCADE" keyword at the end of a "DROP" statement.
    The function in itself is not meant to be called. The @compiles decorator is here to update the SQLAlchemy engine.
    """
    return compiler.visit_drop_table(element) + " CASCADE"


def drop_all_tables() -> None:
    """
    Drop all tables required by mutatron. Nice and simple.
    """
    engine: Engine = EngineSQLA.get_instance()
    meta: alch.MetaData = alch.MetaData()
    meta.reflect(bind=engine)

    # Could also have used MetaData.drop_all()
    tables = meta.tables
    for table_name in tables.keys():
        tables[table_name].drop(engine)


def get_table(table_name: str):
    """
    Get a specific table using its name.

    :param table_name: The name of the table to retrieve
    :type table_name: str
    :return: The table to retrieve
    :rtype: sqlalchemy.Table
    """
    meta: alch.MetaData = alch.MetaData()
    meta.reflect(EngineSQLA.get_instance())
    return meta.tables[table_name]
