import os
from time import time
from typing import List, Dict, TYPE_CHECKING, Optional

import lxml.etree
from lxml import etree

import config
import src.repository.row_management as rm
import src.types as types
from src.os_commands import run

if TYPE_CHECKING:
    from lxml.etree import Element, ElementTree


def _get_module_folders_list(root: str) -> List[str]:
    """
    Retrieve all folder names in the specified root. Root is added to the resulting list.

    :param root: Root to inspect
    :type root: str
    :return: List of folder names
    :rtype: List[str]
    """
    list_folders: List[str] = [
        element.path
        for element in os.scandir(root)
        if os.path.isdir(element)
    ]
    list_folders = [root, *list_folders]
    return list_folders


def extract_test_results(root: str = '.') -> List[Dict[str, str]]:
    """
    Given a module root directory, extract all test results available in XML reports

    :param root: Module root directory
    :type root: str
    :return: List of all test results
    :rtype: List[Dict[str, str]]
    """
    list_folders: List[str] = _get_module_folders_list(root)
    test_results: List[Dict[str, str]] = []
    folder: str
    for folder in list_folders:
        test_results += _extract_test_results_from_folder(folder)
    return test_results


def run_tests(base_timeout_duration: float = 0) -> bool:
    """
    Compile mutant and run `mvn surefire-report:report` to generate required XML
    """
    timeout_duration: Optional[float] = None
    if base_timeout_duration:
        timeout_duration = base_timeout_duration * config.TEST_TIMEOUT_FACTOR
    succeed: bool = run('mvn test', timeout=timeout_duration)
    return succeed


def _get_list_xml_reports(module_path: str) -> List[str]:
    """
    Return a list of all XML test reports in a module given its path.

    :param module_path: Module root path
    :type module_path: str
    :return: List of XML test reports paths
    :rtype: List[str]
    """
    target_path: str = f'{module_path}/target/surefire-reports'
    list_files: List[str] = []
    if os.path.isdir(target_path):
        list_files = os.listdir(target_path)
        list_files = [
            f'{target_path}/{filename}'
            for filename in list_files
            if 'TEST-' in filename
        ]
    return list_files


def _is_well_structured(element: lxml.etree.Element) -> bool:
    """
    Utilitary function to ensure that the element can be used.
    It is necessary in the case of a malformed TEST-* surefire file, in which a testcase can have the same classname and
    name, causing an error when comparing to names available in DB.

    :param element: The element to analyze
    :type element: lxml.etree.Element
    :return: Is the element safe
    :rtype: bool
    """
    attrib: Dict[str, str] = element.attrib
    name: str = attrib['name']
    class_name: str = attrib['classname']
    return name != class_name


def _extract_test_results_from_xml_report(filename: str) -> List[types.TestInfos]:
    """
    Extract test results of a given XML report.

    :param filename: XML report file name
    :type filename: str
    :return: List of all test cases infos
    :rtype: List[types.TestInfos]
    """
    xml_tree: ElementTree = etree.parse(filename)
    test_cases: List[Element] = xml_tree.findall('testcase')
    test_results: Dict[str, types.TestInfos] = {}
    for element in test_cases:
        if _is_well_structured(element):
            infos: types.TestInfos = _extract_element_infos(element)
            test_results[infos['name']] = infos
    return test_results.values()


def _extract_element_infos(element: lxml.etree.Element) -> types.TestInfos:
    """
    Extract relevant infos from an lxml Element object.

    :param element: Element object to analyze
    :type element: lxml.etree.Element
    :return: Dictionnary with all relevant infos
    :rtype: types.TestInfos
    """
    attrib: Dict[str, str] = element.attrib
    # In the case of a parametrized test case, only get the main name
    cleaned_name: str = attrib['name'].split('[')[0]
    class_name: str = attrib['classname']
    name: str = f"{class_name}.{cleaned_name}"
    result: str = 'success'
    if element.findall('error') or element.findall('failure'):
        result = 'fail'
    duration: float = float(attrib['time'])

    element_infos = {
        'name': name,
        'result': result,
        'duration': duration
    }

    return element_infos


def _extract_test_results_from_folder(module_path: str) -> List[Dict[str, str]]:
    """
    Given a module folder, get all the test results which can be found in surefire XML reports.

    :param module_path: Module root folder
    :type module_path: str
    :return: Test results
    :rtype: List[Dict[str, str]]
    """
    folder_name: str = os.path.basename(module_path)

    test_results: List[Dict[str, str]] = []
    if os.path.isdir(module_path):
        list_xml_reports: List[str] = _get_list_xml_reports(module_path)
        filename: str
        test_results: List[Dict[str, str]] = []
        for filename in list_xml_reports:
            report_results: List[types.TestInfos] = _extract_test_results_from_xml_report(filename)
            format_report_results_with_folder_name(report_results, folder_name)
            test_results += report_results
    return test_results


def format_report_results_with_folder_name(report_results: List[types.TestInfos], module_path: str) -> None:
    """
    Format all results name to include the module path. This ensures that all test names are unique.

    :param report_results: Results infos to update
    :type report_results: List[types.TestInfos]
    :param module_path: Module path
    :type module_path: str
    """
    result: types.TestInfos
    for result in report_results:
        result['name'] = f"{module_path}::{result['name']}"


def get_test_names_list(root: str = '.') -> List[str]:
    """
    Retrieve all test names for a given module root.

    :param root: Module root
    :type root: str
    :return: List of all test names
    :rtype: List[str]
    """
    test_results: List[types.TestInfos] = extract_test_results(root)
    test_names: List[str] = [test['name'] for test in test_results]
    return test_names


def run_test(test_class_name: str, test_name: str) -> None:
    """
    Run a specific test based on its class name and its method name.

    :param test_class_name: Name of the class containing the target test.
    :type test_class_name: str
    :param test_name: Name of the test in its test class.
    :type test_name: str
    """
    run('mvn test-compile')
    run(f'mvn -Dtest={test_class_name}#{test_name} surefire:test', mute=False)


def run_pool_test_from_ids(test_ids: List[int]) -> bool:
    """
    Run a specific set of test using their ids.

    :param test_ids: List of test ids to run.
    :type test_ids: List[int]
    """
    dtest_string: str = rm.get_dtest_string(test_ids)
    run('mvn test-compile', mute=False)
    succeed: bool = run(f'mvn "-Dtest={dtest_string}" surefire:test', mute=False)
    return succeed


def get_test_execution_duration() -> float:
    start: float = time()
    run('mvn test')
    duration: float = time() - start
    return duration
