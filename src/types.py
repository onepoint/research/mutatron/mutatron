from typing import Dict, Union

from javalang.tree import ClassDeclaration, InterfaceDeclaration

TestInfos = Dict[str, Union[str, float]]
ClassNode = Union[ClassDeclaration, InterfaceDeclaration]
